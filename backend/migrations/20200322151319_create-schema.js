
exports.up = function(knex) {
  return knex.schema
    .createTable('user', table => {
      table.increments().notNullable();
      table.string('nickname').notNullable();
      table.string('password').notNullable();
      table.string('email').notNullable();
      table.timestamps(true, true);
    })
    .createTable('game', table => {
      table.uuid('id').notNullable().primary();
      table.enu('type', ['duel', 'classic']).notNullable();
      table.integer('duration').notNullable();
      table.timestamps(true, true);
      table.integer('winner_id').references('id').inTable('user').notNullable().onDelete('restrict').onUpdate('cascade');
    })
    .createTable('user_game', table => {
      table.increments();
      table.integer('kills').notNullable();
      table.integer('deaths').notNullable();
      table.timestamps(true, true);
      table.integer('user_id').references('id').inTable('user').notNullable().onDelete('restrict').onUpdate('cascade');
      table.uuid('game_id').references('id').inTable('game').notNullable().onDelete('restrict').onUpdate('cascade');
    })
    .createTable('weapon', table => {
      table.increments().notNullable();
      table.string('name').notNullable();
      table.integer('damage').notNullable();
      table.integer('color').notNullable();
      table.integer('bullet_velocity').notNullable();
      table.integer('bullet_count').notNullable();
      table.float('frequency').notNullable();
      table.integer('range').notNullable();
      table.timestamps(true, true);
    })
    .createTable('weapon_stats', table => {
      table.increments().notNullable();
      table.integer('damage_taken').notNullable();
      table.integer('damage_dealt').notNullable();
      table.timestamps(true, true);
      table.integer('weapon_id').references('id').inTable('weapon').notNullable().onDelete('restrict').onUpdate('cascade');
      table.integer('user_game_id').references('id').inTable('user_game').notNullable().onDelete('restrict').onUpdate('cascade');
    })
};

exports.down = function(knex) {

};
