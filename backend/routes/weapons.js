const express = require('express');
const router = express.Router();
const db = require('../db');

router.get('/', async (req, res, next) => {
  try {
    const weapons = await db('weapon')
      .select(['name', 'damage', 'color', 'bullet_count as bulletCount']);
    
    res.json(weapons);
  } catch (error) {
    return next(error);
  }
});

module.exports = router;
