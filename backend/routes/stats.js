const express = require('express');
const router = express.Router();
const db = require('../db');

router.get('/:playerId', async (req, res, next) => {
  try {
    if (!req.user || +req.params.playerId !== req.user.id) {
      res.status(401).end();
    } else {
      const gamesData = (await db('user_game')
        .count('id as gamesPlayed')
        .sum('kills as kills')
        .sum('deaths as deaths')
        .where('user_id', req.params.playerId))[0];

      const winCount = (await db('game').count('id').where('winner_id', req.params.playerId))[0].count;

      const weaponStats = await db('weapon_stats')
        .select(['weapon.name as weaponName'])
        .sum('damage_taken as damageTaken')
        .sum('damage_dealt as damageDealt')
        .join('weapon', 'weapon.id', 'weapon_stats.weapon_id')
        .join('user_game', 'user_game.id', 'weapon_stats.user_game_id')
        .where('user_id', req.params.playerId)
        .groupBy(['weapon.name', 'weapon_id'])
        .orderBy('weapon_id')
      
      res.json({
        gamesPlayed: parseInt(gamesData.gamesPlayed),
        kills: parseInt(gamesData.kills),
        deaths: parseInt(gamesData.deaths),
        winCount: parseInt(winCount),
        weaponStats: weaponStats.map(e => ({
          weaponName: e.weaponName,
          damageDealt: parseInt(e.damageDealt),
          damageTaken: parseInt(e.damageTaken),
        })),
      });
    }
  } catch (error) {
    return next(error);
  }
});

module.exports = router;
