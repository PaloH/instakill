const express = require('express');
const router = express.Router();
const db = require('../db');

router.get('/', async (req, res, next) => {
  try {
    const data = (await db.raw(`
        select nickname, "winCount", kills, deaths, case deaths when 0 then '∞'::varchar else (kills::float / deaths::float)::numeric(6,2)::varchar end as kdr from (
        select nickname, "winCount", sum(kills) as kills, sum(deaths) as deaths from 
        (select nickname, winner_id, count(winner_id) as "winCount" from game 
        join "user" ON "user"."id" = "game"."winner_id"
        group by winner_id, nickname
        limit 10) as sub
        join user_game on user_game.user_id = sub.winner_id
        group by nickname, "winCount") as sub2
        order by "winCount" DESC, kills DESC, deaths
    `)).rows;
    
    res.json(data);
  } catch (error) {
    return next(error);
  }
});

module.exports = router;
