const express = require('express');
const router = express.Router();
const db = require('../db');
const wsMap = require('../ws-server');

/* GET home page. */
router.get('/', async (req, res) => {
  for([id, ws] of wsMap) {
    console.log(id, ws);
  }
  // wss.clients.forEach(client => {
  //   console.log(client.user);
  // });
  //const user = await db('user').where({ id: 1});
  console.log(req.user);
  res.send('working');
});

module.exports = router;
