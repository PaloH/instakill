const express = require('express');
const router = express.Router();
const db = require('../db');

router.get('/', async (req, res, next) => {
  try {
    if (!req.query.id.match(/^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i)) {
      res.status(400).end();
    } else {
      const game = (await db('game')
        .select(['game.duration', 'game.type', 'user.nickname'])
        .join('user', 'user.id', 'game.winner_id')
        .where('game.id', req.query.id)
      )[0];
      
      if (game) {
        const userStats = await db('user_game')
          .select(['user_game.id', 'kills', 'deaths', 'nickname'])
          .join('user', 'user.id', 'user_game.user_id')
          .where('game_id', req.query.id);

        for (let us of userStats) {
          us.weaponStats = (await db('weapon_stats')
            .sum('damage_taken as damageTaken')
            .sum('damage_dealt as damageDealt')
            .where('user_game_id', us.id))[0];
        }
        
        res.json({ game, userStats });
      } else {
        res.status(404).end();
      }
    }
  } catch (error) {
    return next(error);
  }
});

module.exports = router;
