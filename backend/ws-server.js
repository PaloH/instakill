const WebSocket = require('ws');
const debug = require('debug')('backend:server');
const messageTypes = require('./message-handlers/message-types');
const { openGames, unfinishedGames } = require('./games');
const { leaveGame } = require('./message-handlers/leave-game.handler');

const wss = new WebSocket.Server({ noServer: true });
const wsMap = new Map();

setInterval(() => {
  wsMap.forEach(ws => {
    ws.send(JSON.stringify({
      type: 'ping',
    }));
  });
}, 10000);

wss.on('connection', (ws, req, client) => {
  debug("client connected: ", client);
  wsMap.set(client.id, ws);

  ws.on('message', (message) => {
    const parsedMessage = JSON.parse(message);
    if (parsedMessage.type !== 'sendPosition') debug(`received: ${message} from ${client.email}`);
    if (messageTypes[parsedMessage.type]) {
      // call dedicated message handler
      messageTypes[parsedMessage.type](parsedMessage, client, wsMap);
    } else {
      debug('WARNING: unknown message');
    }
    // ws.send(JSON.stringify({ data: `${client} sent -> ${message}`}));
    // wss.clients.forEach(c => {
    //   if (c !== ws && c.readyState === WebSocket.OPEN) {
    //     c.send(JSON.stringify({ data: `${client} sent -> ${message}`}));
    //   }
    // });
  });

  ws.on('error', (err) => {
    debug(`ws error: ${err}`);
  });

  ws.on('close', () => {
    wsMap.delete(client.id);
    debug(`connection closed with ${client.email}`);

    // delete from unstarted game
    const gameIndex = openGames.findIndex(g => g.players.some(pId => pId === client.id));
    if (gameIndex >= 0) {
      openGames[gameIndex].players.splice(openGames[gameIndex].players.indexOf(client.id), 1);
    } else {
      // delete from unfinished game
      let gameId = null;
      for (const [key, g] of unfinishedGames) {
        if (g.players.some(pId => pId === client.id)) {
          gameId = key;
          break;
        }
      }
      if (gameId) {
        leaveGame(gameId, client.id, wsMap);
      }
    }
  });
});

module.exports = {
    wss,
    wsMap
};