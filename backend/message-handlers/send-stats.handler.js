const debug = require('debug')('backend:server');
const { unfinishedGames } = require('../games');
const db = require('../db');

async function sendStatsHandler({ data }, client, wsMap) {
  const game = unfinishedGames.get(data.gameId);
  if (!game.statsSaved) game.statsSaved = 1;
  else game.statsSaved++;
  
  const userGameId = (await db('user_game').returning(['id']).insert({
    user_id: client.id,
    game_id: data.gameId,
    deaths: data.stats.deaths,
    kills: game.kills[client.id],
  }))[0].id;
  console.log(userGameId);
  for (const weapon of data.stats.weapons) {
    await db('weapon_stats').insert({
      user_game_id: userGameId,
      weapon_id: weapon.id,
      damage_taken: weapon.damage_taken,
      damage_dealt: weapon.damage_dealt,
    });
  }

  if (game.statsSaved === game.players.length) {
    game.players.forEach(playerId => {
      const playerWs = wsMap.get(playerId);
      if (playerWs) {
        playerWs.send(JSON.stringify({
          type: 'statsSavedRedirect',
          data: {},
        }));
      } else {
        debug('WARNING: player disconnected');
      }
    });
    unfinishedGames.delete(data.gameId);
  }
};

module.exports = sendStatsHandler;