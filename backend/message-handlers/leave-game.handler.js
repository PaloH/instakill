const debug = require('debug')('backend:server');
const { unfinishedGames } = require('../games');

function leaveGameHandler({ data }, client, wsMap) {
  leaveGame(data.gameId, client.id, wsMap);
};

function leaveGame(gameId, clientId, wsMap) {
  const game = unfinishedGames.get(gameId);
  game.players.splice(game.players.indexOf(clientId), 1);
  if (game.players.length < 2) {
    game.players.forEach(playerId => {
      const playerWs = wsMap.get(playerId);
      if (playerWs) {
        playerWs.send(JSON.stringify({
          type: 'gameFinishedUnexpectedly',
          data: {}
        }));
      } else {
        debug('WARNING: player disconnected');
      }
    });
    unfinishedGames.delete(game.id);
  } else {
    game.players.forEach(playerId => {
      const playerWs = wsMap.get(playerId);
      if (playerWs) {
        playerWs.send(JSON.stringify({
          type: 'playerDisconnected',
          data: {
            playerId: clientId,
          }
        }));
      } else {
        debug('WARNING: player disconnected');
      }
    });
  }
}

module.exports = {
  leaveGameHandler,
  leaveGame,
}