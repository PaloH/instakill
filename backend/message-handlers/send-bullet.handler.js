const debug = require('debug')('backend:server');

function sendBulletHandler({ data }, client, wsMap) {
  data.to.forEach(playerId => {
    const playerWs = wsMap.get(+playerId);
    if (playerWs) {
      playerWs.send(JSON.stringify({
        type: 'createBullet',
        data: {
          playerId: client.id,
          position: data.position,
          velocity: data.velocity,
          activeWeapon: data.activeWeapon,
        },
      }));
    } else {
      debug('WARNING: player disconnected');
    }
  });
};

module.exports = sendBulletHandler;