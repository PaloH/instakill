const debug = require('debug')('backend:server');
const { performance } = require('perf_hooks');
const { unfinishedGames } = require('../games');
const db = require('../db');

async function sendDeathHandler({ data }, client, wsMap) {
  const game = unfinishedGames.get(data.gameId);
  if (!game) throw 'game id not found in unfinished games';
  if (data.killedById === client.id) game.kills[data.killedById]--;
  else game.kills[data.killedById]++;
  data.to.forEach(playerId => {
    const playerWs = wsMap.get(+playerId);
    if (playerWs) {
      playerWs.send(JSON.stringify({
        type: 'death',
        data: {
          playerId: client.id,
          kills: game.kills,
        },
      }));
    } else {
      debug('WARNING: player disconnected');
    }
  });
  if (game.kills[data.killedById] === 5) {
    console.log('game finished');
    try {
      await db('game').insert({
        id: data.gameId,
        duration: Math.round(performance.now() - game.startedAt),
        type: game.type,
        winner_id: data.killedById,
      });
    } catch (error) {
      throw `Game insertion failed: ${error}`;
    }
    game.players.forEach(playerId => {
      const playerWs = wsMap.get(playerId);
      if (playerWs) {
        playerWs.send(JSON.stringify({
          type: 'gameFinished',
          data: {},
        }));
      } else {
        debug('WARNING: player disconnected');
      }
    });
    game.spawner.stop();
  }
};

module.exports = sendDeathHandler;