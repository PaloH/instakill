const joinGameHandler = require('./join-game.handler');
const sendPositionHandler = require('./send-position.handler');
const sendBulletHandler = require('./send-bullet.handler');
const sendDeathHandler = require('./send-death.handler');
const sendStatsHandler = require('./send-stats.handler');
const { leaveGameHandler } = require('./leave-game.handler');

const messageTypes = {
    'joinGame': joinGameHandler,
    'sendPosition': sendPositionHandler,
    'sendBullet': sendBulletHandler,
    'sendDeath': sendDeathHandler,
    'sendStats': sendStatsHandler,
    'leaveGame': leaveGameHandler,
};

module.exports = messageTypes;
