const debug = require('debug')('backend:server');
const { performance } = require('perf_hooks');
const uuid = require('uuid');
const { gameTypes, unfinishedGames, openGames } = require('../games');
const db = require('../db');
const WeaponSpawner = require('../spawner');

function joinGameHandler({ data }, client, wsMap) {
  const gameType = gameTypes[data.gameType];
  const gameToJoinIndex = openGames.findIndex(g => g.type === data.gameType);
  if (gameToJoinIndex >= 0) {
    const gameToJoin = openGames[gameToJoinIndex];
    if (gameToJoin.players.indexOf(client.id) === -1) {
      gameToJoin.players.push(client.id);
      if (gameToJoin.players.length === gameType.numberOfPlayers) {
        openGames.splice(gameToJoinIndex, 1); // remove element from open games
        startGame(gameToJoin, wsMap);
      }
    } else {
      debug('WARNING: player already in game');
    }
  } else {
    openGames.push({
      type: gameType.name,
      players: [client.id],
    });
  }
};

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

async function startGame(gameToJoin, wsMap) {

  console.log('game to join', gameToJoin);
  // get starting positions
  const positions = {};
  gameToJoin.players.forEach(playerId => {
    positions[playerId] = {
      x: getRandomInt(50, 750),
      y: getRandomInt(50, 550),
    }
  });
  const PLAYER_COLORS = [13776457, 4012185, 14398777, 5027888];
  const players = [];
  for (let i = 0; i < gameToJoin.players.length; i++) {
    players.push({
      id: gameToJoin.players[i],
      nickname: (await db('user').select('nickname').where('id', gameToJoin.players[i]))[0].nickname,
      color: PLAYER_COLORS[i],
    })
  }

  // get weapons
  const weapons = await db('weapon').orderBy('id');
  const gameId = uuid.v4();
  gameToJoin.players.forEach(playerId => {
    const playerWs = wsMap.get(playerId);
    if (playerWs) {
      playerWs.send(JSON.stringify({
        type: 'startGame',
        data: {
          positions,
          weapons,
          myId: playerId,
          gameId,
          players,
        }
      }));
    } else {
      debug('WARNING: player disconnected');
    }
  });
  gameToJoin.kills = {};
  gameToJoin.players.forEach(playerId => {
    gameToJoin.kills[playerId] = 0;
  });

  gameToJoin.spawner = new WeaponSpawner(gameToJoin, wsMap, weapons);
  gameToJoin.spawner.start(5000);

  gameToJoin.startedAt = performance.now();

  unfinishedGames.set(gameId, gameToJoin);
}

module.exports = joinGameHandler;