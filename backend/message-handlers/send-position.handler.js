const debug = require('debug')('backend:server');

function sendPositionHandler({ data }, client, wsMap) {
  data.to.forEach(playerId => {
    const playerWs = wsMap.get(+playerId);
    if (playerWs) {
      playerWs.send(JSON.stringify({
        type: 'updatePosition',
        data: {
          id: client.id,
          x: data.x,
          y: data.y,
          rotation: data.rotation,
        }
      }));
    } else {
      debug('WARNING: player disconnected');
    }
  });
};

module.exports = sendPositionHandler;