
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('weapon').del()
    .then(function () {
      // Inserts seed entries
      return knex('weapon').insert([
        {name: 'fists', damage: 10, range: 20, color: 13814582, frequency: 1.5, bullet_velocity: 1000, bullet_count: -1},
        {name: 'revolver', damage: 20, range: 500, color: 2328433, frequency: 1.5, bullet_velocity: 400, bullet_count: 6},
        {name: 'machine gun', damage: 20, range: 700, color: 13791798, frequency: 4, bullet_velocity: 500, bullet_count: 20},
        {name: 'rifle', damage: 50, range: 1300, color: 6302605, frequency: 0.6, bullet_velocity: 700, bullet_count: 3},
      ]);
    });
};
