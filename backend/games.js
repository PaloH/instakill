const gameTypes = {
  duel: {
    name: 'duel',
    numberOfPlayers: 2,
  },
  classic: {
    name: 'classic',
    numberOfPlayers: 4,
  },
};

const unfinishedGames = new Map();
const openGames = [];

module.exports = {
  gameTypes,
  openGames,
  unfinishedGames,
};
