const LocalStrategy = require('passport-local').Strategy;
const db = require('../db');
const getHashedPassword = require('./get-hashed-password');

const localStrategy = new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password'
}, async function(email, password, done) {
    try {
      console.log('local strategy started');
      const user = (await db('user').where({ email }))[0];
      if (!user) {
        console.log('Email not found.');
        return done(null, false, { message: 'Email not found.' });
      }
      if (user.password !== getHashedPassword(password)) {
        console.log('Incorrect password.');
        return done(null, false, { message: 'Incorrect password.' });
      }
      return done(null, user);
    } catch (err) {
      console.log('failed');
      return done(err);
    }
  }
);

module.exports = localStrategy;