const debug = require('debug')('backend:server');
const db = require('./db');

function WeaponSpawner(game, wsMap, weapons) {
  this.game = game;
  this.wsMap = wsMap;
  this.weapons = weapons;

  this.stopped = false;

  this.start = (waitMs) => {
    if (this.stopped) return;
    setTimeout(() => {
      if (this.stopped) return;
      
      const randomX = Math.floor(Math.random() * (1260 - 20 + 1)) + 20;
      const randomY = Math.floor(Math.random() * (700 - 20 + 1)) + 20;
      const randomWeapon = Math.floor(Math.random() * (this.weapons.length - 1)) + 2;
      this.game.players.forEach(playerId => {
        const playerWs = this.wsMap.get(playerId);
        if (playerWs) {
          playerWs.send(JSON.stringify({
            type: 'spawnWeapon',
            data: {
              x: randomX,
              y: randomY,
              weaponId: randomWeapon,
            }
          }));
        } else {
          debug('WARNING: player disconnected');
        }
      });

      this.start(Math.round(Math.random() * (7000 - 3000)) + 3000);
    }, waitMs);
  };

  this.stop = () => {
    this.stopped = true;
  };
}

module.exports = WeaponSpawner;