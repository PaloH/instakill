import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ValidatorFn, FormGroup, ValidationErrors } from '@angular/forms';
import { AuthService } from '../../auth/auth.service';
import { Router } from '@angular/router';
import { WsService } from 'src/app/ws.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.less']
})
export class RegistrationComponent implements OnInit {

  public emailAlreadyUsed = false;

  passwordMatchValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
    const password = control.get('password');
    const repeatPassword = control.get('repeatPassword');

    return password && repeatPassword && password.value !== repeatPassword.value ? { 'passwordsNotMatching': true } : null;
  };

  registrationForm = this.fb.group({
    email: ['', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
    nickname: ['', Validators.required],
    password: ['', [Validators.required, Validators.minLength(8)]],
    repeatPassword: ['', [Validators.required]]
  }, {validators: this.passwordMatchValidator});

  get email() { return this.registrationForm.get('email'); }
  get nickname() { return this.registrationForm.get('nickname'); }
  get password() { return this.registrationForm.get('password'); }
  get repeatPassword() { return this.registrationForm.get('repeatPassword'); }

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private wsService: WsService,
  ) { }

  ngOnInit(): void {
    this.email.valueChanges.subscribe(() => this.emailAlreadyUsed = false);
  }

  onSubmit() {
    this.registrationForm.markAllAsTouched();

    if (!this.registrationForm.invalid) {
      const data = {
        email: this.email.value,
        nickname: this.nickname.value,
        password: this.password.value,
      }
      this.authService.register(data).subscribe(
        () => {
          this.wsService.createWsConnection();
          this.router.navigate(['/app/new_game']);
        },
        error => {
          if (error.error instanceof ErrorEvent) {
            console.error('An error occurred:', error.error.message);
          } else {
            if (error.status === 409) {
              this.emailAlreadyUsed = true;
            }
            console.error(
              `Backend returned code ${error.status}, ` +
              `body was: ${error.error}`);
          }
        }
      );
    }
  }

}
