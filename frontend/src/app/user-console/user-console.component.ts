import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { WsService } from '../ws.service';

@Component({
  selector: 'app-user-console',
  templateUrl: './user-console.component.html',
  styleUrls: ['./user-console.component.less']
})
export class UserConsoleComponent implements OnInit {
  isCollapsed = true;

  constructor(
    public authService: AuthService,
    private router: Router,
    private wsService: WsService,
  ) { }

  ngOnInit(): void {
  }

  toggleCollapse(): void {
    this.isCollapsed = !this.isCollapsed;
  }

  logout() {
    this.authService.logout().subscribe(
      () => {
        this.wsService.closeConnection();
        this.router.navigate(['/login']);
      },
      err => console.log(err),
    );
  }

}
