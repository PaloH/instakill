import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import * as moment from 'moment';

@Component({
  selector: 'app-finished-game',
  templateUrl: './finished-game.component.html',
  styleUrls: ['./finished-game.component.less']
})
export class FinishedGameComponent implements OnInit {
  public data: any;
  public duration: string;
  public error = false;

  constructor(
    private dataService: DataService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
      this.dataService.getGameData(params.get('id')))
    ).subscribe({
      next: (data) => {
        this.error = false;
        this.data = data;
        this.duration = `${moment.duration(data.game.duration).minutes()} minute(s) ${moment.duration(data.game.duration).seconds()} second(s)`;
        this.data.userStats.sort((a, b) => b.kills - a.kills || b.weaponStats.damageDealt - a.weaponStats.damageDealt || a.deaths - b.deaths);
      },
      error: () => {
        this.error = true;
      }
    });
  }

}
