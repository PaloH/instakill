import { Injectable, OnDestroy, Injector } from '@angular/core';
import { webSocket, WebSocketSubject } from "rxjs/webSocket";
import { MessageTypesOut } from './types/message-types-out.enum';
import { tap } from 'rxjs/operators';
import { MessageTypesIn } from './types/message-types-in.enum';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Subscription, BehaviorSubject, VirtualTimeScheduler } from 'rxjs';
import { GameType } from './types/game-type.enum';
import { AuthService } from './auth/auth.service';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class WsService implements OnDestroy {
  private socket: WebSocketSubject<{ type: MessageTypesIn | MessageTypesOut, data?: any }> = webSocket(environment.wsUrl);
  private subscription: Subscription;
  chosenType = new BehaviorSubject<GameType>(null);

  get isConnected() {
    return this.subscription && !this.subscription.closed;
  }

  constructor(
    private router: Router,
    private injector: Injector,
    private toastr: ToastrService,
  ) { }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.socket.complete();
  }

  public get socketSubject() {
    return this.socket;
    // .pipe(
    //   tap({
    //     next: msg => console.log('message received: ', msg), // Called whenever there is a message from the server.
    //     error: err => console.log(err), // Called if at any point WebSocket API signals some kind of error.
    //     complete: () => console.log('complete') // Called when connection is closed (for whatever reason).
    //   })
    // );
  }

  public createWsConnection() {
    // this.socket.subscribe();
    this.subscription = this.socket.subscribe(
      msg => {
        // console.log('message received: ' + msg); // Called whenever there is a message from the server.
        if (msg.type === MessageTypesIn.START_GAME) {
          console.log('start game');
          this.chosenType.next(null);
          this.router.navigateByUrl('/game', {state: msg.data});
        } else if (msg.type === MessageTypesIn.PING) {
          console.log('ping received');
          this.socket.next({
            type: MessageTypesOut.PONG,
          });
        }
      },
      err => {
        console.log('ws error', err); // Called if at any point WebSocket API signals some kind of error.
        if (this.router.url === '/game') {
          this.router.navigate(['/app/new_game']);
          this.toastr.error('Connection closed for unknown reasons.', 'Connection closed');
        } else {
          const authService = this.injector.get(AuthService);
          authService.checkLogin().subscribe(isLoggedIn => !isLoggedIn && this.router.navigate(['/login']));
        }
      },
      () => {
        console.log('complete'); // Called when connection is closed (for whatever reason).
        if (this.router.url === '/game') {
          this.router.navigate(['/app/new_game']);
          this.toastr.error('Connection closed for unknown reasons.', 'Connection closed');
        } else {
          const authService = this.injector.get(AuthService);
          authService.checkLogin().subscribe(isLoggedIn => !isLoggedIn && this.router.navigate(['/login']));
        }
      }
    );
  }

  public sendMessage(message: { type: MessageTypesOut, data: any }) {
    this.socket.next(message);
  }

  public closeConnection() {
    this.socket.complete();
  }
}
