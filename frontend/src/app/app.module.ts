import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GameComponent } from './game/game.component';
import { LoginComponent } from './login-register-wrapper/login/login.component';
import { UserConsoleComponent } from './user-console/user-console.component';
import { NewGameComponent } from './new-game/new-game.component';
import { StatsComponent } from './stats/stats.component';
import { LeaderboardsComponent } from './leaderboards/leaderboards.component';
import { FinishedGameComponent } from './finished-game/finished-game.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginRegisterWrapperComponent } from './login-register-wrapper/login-register-wrapper.component';
import { RegistrationComponent } from './login-register-wrapper/registration/registration.component';
import { ToastrModule } from 'ngx-toastr';
import { AboutComponent } from './about/about.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    GameComponent,
    LoginComponent,
    UserConsoleComponent,
    NewGameComponent,
    StatsComponent,
    LeaderboardsComponent,
    FinishedGameComponent,
    LoginRegisterWrapperComponent,
    RegistrationComponent,
    AboutComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
