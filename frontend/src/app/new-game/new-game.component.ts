import { Component, OnInit } from '@angular/core';
import { GameType } from '../types/game-type.enum';
import { WsService } from '../ws.service';
import { MessageTypesOut } from '../types/message-types-out.enum';
import { DataService } from '../data.service';

@Component({
  selector: 'app-new-game',
  templateUrl: './new-game.component.html',
  styleUrls: ['./new-game.component.less']
})
export class NewGameComponent implements OnInit {
  duel: GameType = GameType.DUEL;
  classic: GameType = GameType.CLASSIC;

  constructor(
    public wsService: WsService,
  ) { }

  ngOnInit(): void {
    this.wsService.socketSubject.subscribe()
  }

  joinGame(gameType: GameType) {
    if (!this.wsService.chosenType.value) {
      this.wsService.chosenType.next(gameType);
      this.wsService.sendMessage({
        type: MessageTypesOut.JOIN_GAME,
        data: { gameType },
      });
    }
  }

}
