import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LeaderboardsDto, FinishedGameDto, StatsDto, WeaponDto } from './types/dto.models';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  public getGameData(gameId: string): Observable<FinishedGameDto> {
    const options = {
      params: new HttpParams().set('id', gameId),
    };
    return this.http.get<FinishedGameDto>('/api/game', options);
  }

  public getLeaderBoard(): Observable<LeaderboardsDto> {
    return this.http.get<LeaderboardsDto>('/api/leaderboard');
  }

  public getStats(playerId: number): Observable<StatsDto> {
    return this.http.get<StatsDto>(`/api/stats/${playerId}`);
  }

  public getWeapons(): Observable<WeaponDto[]> {
    return this.http.get<WeaponDto[]>('/api/weapons');
  }
}
