import { Component, OnInit } from '@angular/core';
import { LeaderboardsDto } from '../types/dto.models';
import { DataService } from '../data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-leaderboards',
  templateUrl: './leaderboards.component.html',
  styleUrls: ['./leaderboards.component.less']
})
export class LeaderboardsComponent implements OnInit {
  leaderboardsData$: Observable<LeaderboardsDto>;

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.leaderboardsData$ = this.dataService.getLeaderBoard();
  }

}
