import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { WsService } from '../ws.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  userSubject = new BehaviorSubject<{id: number, nickname: string, email: string}>(null);

  constructor(
    private http: HttpClient,
    private wsService: WsService,
  ) { }

  register(data): Observable<void> {
    return this.http.post<void>('/api/auth/registration', data);
  }

  login(data): Observable<void> {
    return this.http.post<void>('/api/auth/login', data);
  }

  logout(): Observable<void> {
    this.wsService.chosenType.next(null);
    return this.http.get<void>('/api/auth/logout');
  }

  checkLogin(): Observable<boolean> {
    return this.http.get<{id: number, nickname: string, email: string}>('/api/auth/checkLogin').pipe(
      map((user) => {
        if (user) {
          if (!this.wsService.isConnected) {
            this.wsService.createWsConnection();
          }
          this.userSubject.next(user);
          return true;
        }
        return false;
      }),
    );
  }
}
