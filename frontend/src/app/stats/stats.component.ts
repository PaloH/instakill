import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { StatsDto } from '../types/dto.models';
import { DataService } from '../data.service';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.less']
})
export class StatsComponent implements OnInit {
  public statsData: StatsDto

  constructor(
    private authService: AuthService,
    private dataService: DataService,
  ) { }

  ngOnInit(): void {
    this.dataService.getStats(this.authService.userSubject.value.id).subscribe({
      next: (data) => this.statsData = data,
    });
  }

}
