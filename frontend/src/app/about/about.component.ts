import { Component, OnInit } from '@angular/core';
import { WeaponDto } from '../types/dto.models';
import { Observable } from 'rxjs';
import { DataService } from '../data.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.less']
})
export class AboutComponent implements OnInit {

  public weapons$: Observable<WeaponDto[]>
  public showBackBtn = !window.location.href.includes('app');

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.weapons$ = this.dataService.getWeapons();
  }

}
