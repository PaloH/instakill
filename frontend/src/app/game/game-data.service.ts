import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GameType } from '../types/game-type.enum';

@Injectable({
  providedIn: 'root'
})
export class GameDataService {

  constructor(private http: HttpClient) { }

  public joinGame(type: GameType): Observable<any> {
    const options = {
      params: new HttpParams().set('type', type),
    };
    return this.http.get<any>('/api/game/join', options);
  }
}
