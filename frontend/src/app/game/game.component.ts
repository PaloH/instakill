import { Component, OnInit, OnChanges, NgZone } from '@angular/core';
import * as Phaser from 'phaser';
import { MainScene, STARTING_AMMO } from './main-scene/main.scene';
import { WsService } from '../ws.service';
import { filter } from 'rxjs/operators';
import { MessageTypesIn } from '../types/message-types-in.enum';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { MessageTypesOut } from '../types/message-types-out.enum';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.less'],
  providers: [MainScene]
})
export class GameComponent implements OnInit {

  phaserGame: Phaser.Game;
  gameConfig: Phaser.Types.Core.GameConfig = {
    type: Phaser.AUTO,
    scale: {
      parent: 'canvas-wrapper',
      mode: Phaser.Scale.FIT,
      autoCenter: Phaser.Scale.CENTER_BOTH,
      width: 1280,
      height: 720,
    },
    backgroundColor: '0x111111',
    physics: {
      default: 'arcade',
      arcade: {
        // debug: true,
        gravity: { y: 0 }
      }
    }
  };

  hp = 100;
  players: {
    id: number;
    nickname: string;
    color: number;
    kills: number;
  }[];
  weapons: {
    id: number;
    name: string;
    color: number;
    ammo: number;
  }[];
  activeWeapon = 1;
  Infinity = Infinity;

  constructor(
    private mainScene: MainScene,
    private ngZone: NgZone,
    private wsService: WsService,
    private authService: AuthService,
    private router: Router,
  ) { }

  ngOnInit() {
    const initData = window.history.state;
    this.players = initData.players.map(p => ({
      ...p,
      kills: 0,
    }));
    this.weapons = initData.weapons.map(w => ({
      id: w.id,
      name: w.name,
      color: w.color,
      ammo: STARTING_AMMO[w.name] || 0,
    }));
    this.startGame(initData);
  }

  startGame(data: any) {
    this.ngZone.runOutsideAngular(() => {
      this.phaserGame = new Phaser.Game(this.gameConfig);
      this.phaserGame.scene.add('main', this.mainScene, true, data);
      this.mainScene.setGameComponent(this);
    });
  }

  logout() {
    this.authService.logout().subscribe(
      () => {
        this.wsService.closeConnection();
        this.router.navigate(['/login']);
        this.phaserGame.destroy(true);
      },
      err => console.log(err),
    );
  }

  leaveGame() {
    this.phaserGame.destroy(true);
    this.wsService.sendMessage({
      type: MessageTypesOut.LEAVE_GAME,
      data: {
        gameId: window.history.state.gameId,
      }
    });
    this.router.navigate(['/app/new_game']);
  }
}
