import * as Phaser from 'phaser';
import { Weapon } from 'src/app/types/weapon.type';
import { Textures } from 'src/app/types/textures.enum';

export class WeaponSprite extends Phaser.Physics.Arcade.Sprite {
  public weapon: Weapon;

  constructor(scene: Phaser.Scene, x: number, y: number, weapon: Weapon) {
    super(scene, x, y, Textures.WEAPON);
    this.weapon = weapon;

    scene.add.existing(this);
    scene.physics.add.existing(this);

    this.tint = weapon.color;
    this.setDisplaySize(20, 20);
  }
}
