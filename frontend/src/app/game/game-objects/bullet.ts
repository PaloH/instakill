import * as Phaser from 'phaser';
import { Textures } from 'src/app/types/textures.enum';
import { Weapon } from 'src/app/types/weapon.type';

export class BulletSprite extends Phaser.Physics.Arcade.Sprite {
  weapon: Weapon;
  playerId: number;

  constructor(
    scene: Phaser.Scene,
    x: number,
    y: number,
    velocityX: number,
    velocityY: number,
    playerId: number,
    weapon: Weapon,
    size: number
  ) {
    super(scene, x, y, Textures.BULLET);
    this.playerId = playerId;
    this.weapon = weapon;

    scene.add.existing(this);
    scene.physics.add.existing(this);

    this.tint = weapon.color;
    this.setDisplaySize(size, size);
    this.setVelocityX(velocityX);
    this.setVelocityY(velocityY);
    this.setCollideWorldBounds(true, 1, 1);
  }
}
