import * as Phaser from 'phaser';
import { Textures } from 'src/app/types/textures.enum';

const PLAYER_SIZE = 40;

export class PlayerSprite extends Phaser.Physics.Arcade.Sprite {
  id: number;
  color: number;
  isDead = false;

  constructor(scene: Phaser.Scene, x: number, y: number, color: number, id: number) {
    super(scene, x, y, Textures.PLAYER);
    this.id = id;
    this.color = color;

    scene.add.existing(this);
    scene.physics.add.existing(this);

    this.tint = color;
    this.setDisplaySize(PLAYER_SIZE, PLAYER_SIZE);
    this.setCollideWorldBounds(true);
  }
}
