import * as Phaser from 'phaser';
import { ToastrService } from 'ngx-toastr';
import { Injectable, OnDestroy } from '@angular/core';
import { WsService } from 'src/app/ws.service';
import { MessageTypesIn } from 'src/app/types/message-types-in.enum';
import { MessageTypesOut } from 'src/app/types/message-types-out.enum';
import { Textures } from 'src/app/types/textures.enum';
import { Weapon } from 'src/app/types/weapon.type';
import { WeaponSprite } from '../game-objects/weapon';
import { PlayerSprite } from '../game-objects/player';
import { Subscription } from 'rxjs';
import { BulletSprite } from '../game-objects/bullet';
import { Router } from '@angular/router';
import { GameComponent } from '../game.component';

const PLAYER_SPEED = 160;
const PLAYER_SIZE = 40;
const BULLET_SIZE = 10;
export const STARTING_AMMO = {
  fists: Infinity,
  revolver: 6,
};
const DEAD_COLOR = 0x222222;
const DEAD_TIME = 3000;

@Injectable()
export class MainScene extends Phaser.Scene implements OnDestroy {
  player: PlayerSprite;
  playersColors: Map<number, number>;
  positions: { [key: string]: {x: number, y: number, rotation: number } };
  weapons: Weapon[];
  myId: number;
  gameId: string;
  otherPlayers: {
    [key: string]: PlayerSprite
  } = {};
  playerData: {
    hp: number;
    weapons: {
      id: number;
      name: string;
      ammo: number;
      damage_taken: number;
      damage_dealt: number;
    }[];
    activeWeapon: number;
    deaths: number;
  };
  pointer: Phaser.Physics.Arcade.Sprite;
  cursors: {
    left: Phaser.Input.Keyboard.Key,
    right: Phaser.Input.Keyboard.Key,
    up: Phaser.Input.Keyboard.Key,
    down: Phaser.Input.Keyboard.Key,
  };
  wsSubscription: Subscription;
  gameComponent: GameComponent;
  lastFired = performance.now();

  public setGameComponent(gc: GameComponent) {
    this.gameComponent = gc;
  }

  private get notEmptyWeaponsIndexes(): number[] {
    return this.playerData.weapons.reduce((acc, cur, i) => ((cur.ammo > 0 && acc.push(i)), acc), []);
  }

  private defineKeys(){
    this.cursors = {
      left: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A),
      right: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D),
      up: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W),
      down: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S),
    }
  }

  private isAlive(bullet: BulletSprite, player: PlayerSprite) {
    return !player.isDead;
  }

  private enemyHitCallback(bullet: BulletSprite, player: PlayerSprite) {
    console.log('enemy hit', bullet.weapon.name, player.id, this);
    if (bullet.playerId === this.myId) { // my bullet hit someone
      const thisWeaponData = this.playerData.weapons.find(w => w.id === bullet.weapon.id);
      thisWeaponData.damage_dealt += bullet.weapon.damage;
    }
    if (player.id === this.myId) { // some bullet hit me
      const thisWeaponData = this.playerData.weapons.find(w => w.id === bullet.weapon.id);
      thisWeaponData.damage_taken += bullet.weapon.damage;
      this.playerData.hp -= bullet.weapon.damage;
      this.gameComponent.hp = this.playerData.hp;
      if (this.playerData.hp <= 0) {
        this.playerData.deaths++;

        this.player.tint = DEAD_COLOR;
        this.player.isDead = true;
        this.playerData.weapons = this.playerData.weapons.map(w => ({
          ...w,
          ammo: STARTING_AMMO[w.name] || 0,
        }));
        this.gameComponent.weapons = this.gameComponent.weapons.map(w => ({
          ...w,
          ammo: STARTING_AMMO[w.name] || 0,
        }));
        this.playerData.activeWeapon = 1;
        this.gameComponent.activeWeapon = 1;
        setTimeout(() => {
          this.player.x = Math.floor(Math.random() * (760 - 40 + 1)) + 40;
          this.player.y = Math.floor(Math.random() * (560 - 40 + 1)) + 40;
          this.player.tint = this.player.color;
          this.player.isDead = false;
          this.playerData.hp = this.gameComponent.hp = 100;
        }, DEAD_TIME);

        this.wsService.sendMessage({
          type: MessageTypesOut.SEND_DEATH,
          data: {
            killedById: bullet.playerId,
            to: [...Object.keys(this.otherPlayers), this.myId],
            gameId: this.gameId,
          }
        })
      }
    }
    bullet.destroy();
    console.log(this.playerData);
  }

  private takeWeapon(weapon: WeaponSprite, player: PlayerSprite) {
    console.log('take weapon');
    weapon.destroy();

    if (this.myId === player.id) {
      console.log('adding ammo');
      this.playerData.weapons.find(w => w.id === weapon.weapon.id).ammo += weapon.weapon.bullet_count;
      this.gameComponent.weapons.find(w => w.id === weapon.weapon.id).ammo += weapon.weapon.bullet_count;
    }
  }

  private handleMouseWheel(event) { // weapon switching
    if (event.deltaY > 0) {
      console.log('wheel down');
      if (this.playerData.activeWeapon === 0) {
        this.playerData.activeWeapon = this.notEmptyWeaponsIndexes[this.notEmptyWeaponsIndexes.length - 1];
      } else {
        this.playerData.activeWeapon = this.notEmptyWeaponsIndexes.reduce((acc, cur) => cur < this.playerData.activeWeapon && cur > acc ? cur : acc, 0);
      }
    } else {
      console.log('wheel up');
      const nextWeapon = this.notEmptyWeaponsIndexes.find(i => i > this.playerData.activeWeapon);
      this.playerData.activeWeapon = nextWeapon ? nextWeapon : 0;
    }
    this.gameComponent.activeWeapon = this.playerData.activeWeapon;
  };

  private fire() {
    this.playerData.weapons[this.playerData.activeWeapon].ammo -= 1;
    this.gameComponent.weapons[this.playerData.activeWeapon].ammo -= 1;
    const curWeapon = this.weapons[this.playerData.activeWeapon];
    const direction = Math.atan( (this.pointer.x - this.player.x) / (this.pointer.y-this.player.y));
    const speed = this.pointer.y >= this.player.y ? curWeapon.bullet_velocity : -curWeapon.bullet_velocity;
    let offset = Math.hypot(PLAYER_SIZE / 2, PLAYER_SIZE / 2) + Math.hypot(BULLET_SIZE / 2, BULLET_SIZE / 2) + 1;
    offset = this.pointer.y >= this.player.y ? offset : -offset;
    const bulletX = this.player.x + offset * Math.sin(direction);
    const bulletY = this.player.y + offset * Math.cos(direction);
    const bullet = new BulletSprite(
      this,
      bulletX,
      bulletY,
      speed * Math.sin(direction),
      speed * Math.cos(direction),
      this.myId,
      curWeapon,
      BULLET_SIZE
    );
    this.physics.add.overlap(bullet, Object.values(this.otherPlayers), this.enemyHitCallback, this.isAlive, this);
    this.physics.add.overlap(bullet, this.player, this.enemyHitCallback, this.isAlive, this);
    this.wsService.sendMessage({
      type: MessageTypesOut.SEND_BULLET,
      data: {
        position: { x: bulletX, y: bulletY },
        velocity: bullet.body.velocity,
        activeWeapon: this.playerData.activeWeapon,
        to: Object.keys(this.otherPlayers),
      }
    });

    setTimeout(() => {
      bullet.destroy();
    }, curWeapon.range / curWeapon.bullet_velocity * 1000);

    if (this.playerData.weapons[this.playerData.activeWeapon].ammo == 0) { // switch to lower weapon
      this.playerData.activeWeapon = this.notEmptyWeaponsIndexes.reduce((acc, cur) => cur < this.playerData.activeWeapon && cur > acc ? cur : acc, 0);
      this.gameComponent.activeWeapon = this.playerData.activeWeapon;
    }
  }

  constructor(
    private wsService: WsService,
    private router: Router,
    private toastr: ToastrService
  ) {
    super({ key: 'main' });

    this.wsSubscription = this.wsService.socketSubject.subscribe({
      next: (msg) => {
        if (msg.type === MessageTypesIn.UPDATE_POSITION && this.positions) {
          this.positions[msg.data.id].x = msg.data.x;
          this.positions[msg.data.id].y = msg.data.y;
          this.positions[msg.data.id].rotation = msg.data.rotation;
        } else if (msg.type === MessageTypesIn.CREATE_BULLET) {
          const curWeapon = this.weapons[msg.data.activeWeapon];
          const bullet = new BulletSprite(
            this,
            msg.data.position.x,
            msg.data.position.y,
            msg.data.velocity.x,
            msg.data.velocity.y,
            msg.data.playerId,
            curWeapon,
            BULLET_SIZE
          );
          this.physics.add.overlap(bullet, Object.values(this.otherPlayers), this.enemyHitCallback, this.isAlive, this);
          this.physics.add.overlap(bullet, this.player, this.enemyHitCallback, this.isAlive, this);
          setTimeout(() => {
            bullet.destroy();
          }, curWeapon.range / curWeapon.bullet_velocity * 1000);
        } else if (msg.type === MessageTypesIn.SPAWN_WEAPON) {
          console.log('spawn');
          const weapon = new WeaponSprite(this, msg.data.x, msg.data.y, this.weapons.find(w => w.id === msg.data.weaponId));
          this.physics.add.overlap(weapon, Object.values(this.otherPlayers), this.takeWeapon, null, this);
          this.physics.add.overlap(weapon, this.player, this.takeWeapon, null, this);
        } else if (msg.type === MessageTypesIn.DEATH) {
          this.gameComponent.players = Object.entries(msg.data.kills).map(([id, kills]) => ({
            ...this.gameComponent.players.find(p => p.id === +id),
            kills: kills as number,
          }));
          if (this.otherPlayers[msg.data.playerId]) {
            this.otherPlayers[msg.data.playerId].tint = DEAD_COLOR;
            this.otherPlayers[msg.data.playerId].isDead = true;
            setTimeout(() => {
              this.otherPlayers[msg.data.playerId].isDead = false;
              this.otherPlayers[msg.data.playerId].tint = this.otherPlayers[msg.data.playerId].color;
            }, DEAD_TIME);
          }
        } else if (msg.type === MessageTypesIn.GAME_FINISHED) {
          this.wsService.sendMessage({
            type: MessageTypesOut.SEND_STATS,
            data: {
              gameId: this.gameId,
              stats: {
                weapons: this.playerData.weapons,
                deaths: this.playerData.deaths,
              }
            }
          });
        } else if (msg.type === MessageTypesIn.STATS_SAVED_REDIRECT) {
          this.sys.game.destroy(true);
          setTimeout(() => this.router.navigate([`app/game/${this.gameId}`]));
        } else if (msg.type === MessageTypesIn.PLAYER_DISCONNECTED) {
          this.otherPlayers[msg.data.playerId].destroy();
          delete this.positions[msg.data.playerId];
          delete this.otherPlayers[msg.data.playerId];
        } else if (msg.type === MessageTypesIn.GAME_FINISHED_UNEXPECTEDLY) {
          this.toastr.error('All other players disconnected', 'Game finished');
          this.sys.game.destroy(true);
          setTimeout(() => this.router.navigate([`app/new_game`]));
        }
      }
    })
  }

  ngOnDestroy() {
    window.removeEventListener('wheel', this.handleMouseWheel);
    this.wsSubscription.unsubscribe();
  }

  init(data) {
    console.log('initial data', JSON.stringify(data, null, 2));
    this.playersColors = new Map();
    data.players.forEach(p => this.playersColors.set(p.id, p.color));
    this.positions = data.positions;
    this.weapons = data.weapons;
    this.myId = data.myId;
    this.gameId = data.gameId;

    this.playerData = {
      hp: 100,
      weapons: this.weapons.map(weapon => ({
        id: weapon.id,
        name: weapon.name,
        ammo: STARTING_AMMO[weapon.name] || 0,
        damage_taken: 0,
        damage_dealt: 0,
      })),
      activeWeapon: 1,
      deaths: 0,
    }
  }

  preload() {
    console.log('preload method');
    this.load.image(Textures.CROSSHAIR, 'assets/crosshair.png');
    this.load.image(Textures.PLAYER, 'assets/player.png');
    this.load.image(Textures.BULLET, 'assets/bullet.png');
    this.load.image(Textures.WEAPON, 'assets/weapon.png');
  }

  create() {
    console.log('create method');
    this.cameras.main.backgroundColor.setTo(240,240,240);

    Object.entries(this.positions).forEach(([id, {x, y}]) => {
      if (+id === this.myId) {
        this.player = new PlayerSprite(this, x, y, this.playersColors.get(+id), +id);
      } else {
        this.otherPlayers[id] = new PlayerSprite(this, x, y, this.playersColors.get(+id), +id);
      }
    });
    delete this.positions[this.myId]; // remove my position so positions store only other players positions

    this.pointer = this.physics.add.sprite(0, 0, Textures.CROSSHAIR);
    this.pointer.setOrigin(0.5, 0.5).setDisplaySize(25, 25).setCollideWorldBounds(true);

    window.addEventListener("wheel", this.handleMouseWheel.bind(this));

    // Pointer lock will only work after mousedown
    this.game.canvas.addEventListener('mousedown', () => {
      this.game.input.mouse.requestPointerLock();
    });

    // Exit pointer lock when Q or escape (by default) is pressed.
    this.input.keyboard.on('keydown_Q', (event) => {
      if (this.game.input.mouse.locked)
        this.game.input.mouse.releasePointerLock();
    }, this);

    // Move reticle upon locked pointer move
    this.input.on('pointermove', function (p) {
      if (this.input.mouse.locked) {

        this.pointer.x += p.movementX;
        this.pointer.y += p.movementY;
      }
    }, this);

    this.defineKeys();
  }

  counter = 0

  update() {
    this.counter++;
    if (!this.player.isDead) {
      if (this.cursors.left.isDown) {
        this.player.setVelocityX(-PLAYER_SPEED);
      } else if (this.cursors.right.isDown) {
        this.player.setVelocityX(PLAYER_SPEED);
      } else {
        this.player.setVelocityX(0);
      }

      if (this.cursors.up.isDown) {
        this.player.setVelocityY(-PLAYER_SPEED);
      } else if (this.cursors.down.isDown) {
        this.player.setVelocityY(PLAYER_SPEED);
      } else {
        this.player.setVelocityY(0);
      }
    } else {
      this.player.setVelocityX(0);
      this.player.setVelocityY(0);
    }

    this.player.rotation = Phaser.Math.Angle.Between(this.player.x, this.player.y, this.pointer.x, this.pointer.y);

    if (this.input.activePointer.isDown) {
      const activeWeaponFrequency = this.weapons.find((w) => w.id === this.playerData.weapons[this.playerData.activeWeapon].id).frequency;
      const now = performance.now();
      if (now - this.lastFired > 1000 / activeWeaponFrequency && !this.player.isDead) {
        this.lastFired = now;
        this.fire();
      }
    }

    // update enemy's positions
    Object.entries(this.positions).forEach(([id, {x, y, rotation}]) => {
      // console.log(this.otherPlayers[id], x, y);
      this.otherPlayers[id].setPosition(x, y);
      this.otherPlayers[id].rotation = rotation;
    });

    this.wsService.sendMessage({
      type: MessageTypesOut.SEND_POSITION,
      data: {
        x: this.player.x,
        y: this.player.y,
        rotation: this.player.rotation,
        to: Object.keys(this.otherPlayers),
      }
    });
  }
}
