export type Weapon = {
  id: number;
  name: string;
  range: number;
  damage: number;
  frequency: number;
  color: number;
  bullet_count: number;
  bullet_velocity: number;
}
