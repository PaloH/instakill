export interface LeaderboardsDto {
  nickname: string;
  winCount: string;
  kills: string;
  deaths: string;
  kdr: string;
}

export interface FinishedGameDto {
  game: {
    duration: number;
    type: string;
    nickname: string;
  },
  userStats: {
    id: number;
    kills: number;
    deaths: number;
    nickname: string;
    weaponStats: {
      damageTaken: string;
      damageDealt: string;
    }
  }[];
}

export interface StatsDto {
  winCount: number;
  gamesPlayed: number;
  kills: number;
  deaths: number;
  weaponStats: {
    weaponName: string;
    damageDealt: number;
    damageTaken: number;
  }[];
}

export interface WeaponDto {
  name: string;
  damage: number;
  color: number;
  bulletCount: number;
}
