export enum Textures {
  CROSSHAIR = 'CROSSHAIR',
  PLAYER = 'PLAYER',
  BULLET = 'BULLET',
  WEAPON = 'WEAPON',
}
