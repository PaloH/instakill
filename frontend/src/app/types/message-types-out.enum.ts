export enum MessageTypesOut {
  JOIN_GAME = 'joinGame',
  SEND_POSITION = 'sendPosition',
  SEND_BULLET = 'sendBullet',
  SEND_DEATH = 'sendDeath',
  SEND_STATS = 'sendStats',
  LEAVE_GAME = 'leaveGame',
  PONG = 'pong',
}
