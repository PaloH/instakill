export enum MessageTypesIn {
  START_GAME = 'startGame',
  UPDATE_POSITION = 'updatePosition',
  CREATE_BULLET = 'createBullet',
  SPAWN_WEAPON = 'spawnWeapon',
  DEATH = 'death',
  GAME_FINISHED = 'gameFinished',
  STATS_SAVED_REDIRECT = 'statsSavedRedirect',
  PLAYER_DISCONNECTED = 'playerDisconnected',
  GAME_FINISHED_UNEXPECTEDLY = 'gameFinishedUnexpectedly',
  PING = 'ping',
}
